package hudson.plugins.openshift_wrapper;

import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.utils.IOUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;

public class Archiver {

	public static void compress(String inputDir, String outputTarGzArchive) throws CompressorException, ArchiveException, IOException {
		File inputDirFile = new File(inputDir);
		File outputArchiveFile = new File(outputTarGzArchive);
		outputArchiveFile.createNewFile();

		FileOutputStream fos = new FileOutputStream(outputArchiveFile);

		TarArchiveOutputStream taos = new TarArchiveOutputStream(new GZIPOutputStream(new BufferedOutputStream(fos)));
		taos.setBigNumberMode(TarArchiveOutputStream.BIGNUMBER_STAR);
		taos.setLongFileMode(TarArchiveOutputStream.LONGFILE_GNU);

		for(File f : inputDirFile.listFiles()) {
			addFiles(taos, f, "");
		}
		taos.close();
	}

	private static void addFiles(TarArchiveOutputStream taos, File file, String dir) throws IOException {
		taos.putArchiveEntry(new TarArchiveEntry(file, dir + File.separator + file.getName()));

		if (file.isFile()) {
			BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
			IOUtils.copy(bis, taos);
			taos.closeArchiveEntry();
			bis.close();
		}

		else if (file.isDirectory()) {
			taos.closeArchiveEntry();
			for (File childFile : file.listFiles()) {
				addFiles(taos, childFile, dir + File.separator + file.getName());
			}
		}
	}
}

