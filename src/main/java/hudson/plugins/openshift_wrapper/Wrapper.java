package hudson.plugins.openshift_wrapper;

import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;

public class Wrapper {
	interface Structure {
		String BUILD_DEPENDENCIES = "/build_dependencies/";
		String WEBAPPS = "/dependencies/jbossews/webapps/";
		String MARKERS = "/repo/.openshift/markers/";
		String CONFIG = "/repo/.openshift/config/";
	}

	private static final String CONTEXT_REPLACED = "/context_propertyReplaced.xml";
	private static final String TMP = "/tmpStructureDir/";

	private static final String CONTEXT = "context.xml";
	private static final String WEBAPP = "ROOT.war";

	private final PrintStream logger;
	private final String workspace;
	private final String tmpDir;

	public Wrapper(PrintStream logger, String workspace) {
		this.logger = logger;
		this.workspace = workspace;
		this.tmpDir = workspace + TMP;
	}

	public void wrap(String context, String war, String archive, List<Marker> markers, String properties) throws IOException, ArchiveException, CompressorException, ClassNotFoundException {
		deleteTomcatOpenshiftStructure(tmpDir);
		createTomcatOpenshiftStructure(tmpDir);

		if (markers != null) {
			for (Marker marker : markers) {
				createMarker(tmpDir, marker);
			}
		}
		if (context != null && !context.isEmpty()) {
			if (properties != null && !properties.isEmpty()) {
				replaceProperties(properties, context);
				moveConfig(tmpDir, CONTEXT_REPLACED, CONTEXT);

			} else {
				copyConfig(tmpDir, context, CONTEXT);
			}
		}

		copyWebapp(tmpDir, war, WEBAPP);

		createArchive(tmpDir, archive);
		deleteTomcatOpenshiftStructure(tmpDir);
	}

	private void createTomcatOpenshiftStructure(String rootDirectory) {
		logger.println("Creating Openshift Tomcat temporary structure:");
		logger.printf("* %s\n", Structure.BUILD_DEPENDENCIES);
		ensureSuccess(new File(rootDirectory + Structure.BUILD_DEPENDENCIES).mkdirs());
		logger.printf("* %s\n", Structure.WEBAPPS);
		ensureSuccess(new File(rootDirectory + Structure.WEBAPPS).mkdirs());
		logger.printf("* %s\n", Structure.MARKERS);
		ensureSuccess(new File(rootDirectory + Structure.MARKERS).mkdirs());
		logger.printf("* %s\n", Structure.CONFIG);
		ensureSuccess(new File(rootDirectory + Structure.CONFIG).mkdirs());
	}

	private void createMarker(String rootDirectory, Marker marker) throws IOException {
		logger.printf("Create marker '%s'\n", marker.getMark());
		ensureSuccess(new File(rootDirectory + Structure.MARKERS + marker.getMark()).createNewFile());
	}

	private void deleteTomcatOpenshiftStructure(String rootDirectory) throws IOException {
		logger.println("Deleting Openshift Tomcat temporary structure");
		FileUtils.deleteDirectory(new File(rootDirectory));
	}

	private void copyConfig(String rootDirectory, String config, String as) throws IOException {
		logger.printf("Copying configuration file as '%s'\n", as);
		FileUtils.copyFile(new File(onWorkspace(config)), new File(rootDirectory + Structure.CONFIG + as));
	}

	private void moveConfig(String rootDirectory, String config, String as) throws IOException {
		logger.printf("Moving configuration file as '%s'\n", as);
		FileUtils.moveFile(new File(onWorkspace(config)), new File(rootDirectory + Structure.CONFIG + as));
	}

	private void copyWebapp(String rootDirectory, String war, String as) throws IOException {
		logger.printf("Copying web application as '%s'\n", as);
		FileUtils.copyFile(findCorrectFile(onWorkspace(war)), new File(rootDirectory + Structure.WEBAPPS + as));
	}

	private void createArchive(String rootDirectory, String archive) throws IOException, ArchiveException, CompressorException {
		logger.printf("Creating Openshift Tomcat archive as '%s'\n", archive);
		Archiver.compress(rootDirectory, onWorkspace(archive));
	}

	private void ensureSuccess(boolean success) {
		if (!success) {
			throw new RuntimeException("Could not read or write on filesystem");
		}
	}

	private void replaceProperties(String properties, String context) throws IOException, ClassNotFoundException {
		logger.printf("Replacing context properties from '%s'\n", properties);
		PropertyReplacer.replaceProperties(onWorkspace(properties), onWorkspace(context), onWorkspace(CONTEXT_REPLACED));
	}

	private String onWorkspace(String file) {
		return workspace + File.separator + file;
	}

	private File findCorrectFile(String pattern) throws FileNotFoundException {
		int separatorPosition = pattern.lastIndexOf(File.separator);
		String directory = pattern.substring(0, separatorPosition);
		String filePattern = pattern.substring(separatorPosition + 1);

		File dir = new File(directory);
		FileFilter fileFilter = new WildcardFileFilter(filePattern);
		for (File file : dir.listFiles(fileFilter)) {
			return file;
		}
		throw new FileNotFoundException(pattern);
	}
}
