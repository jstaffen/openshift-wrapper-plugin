package hudson.plugins.openshift_wrapper;

import hudson.Extension;
import hudson.Launcher;
import hudson.model.AbstractBuild;
import hudson.model.BuildListener;
import hudson.tasks.BuildStepDescriptor;
import hudson.tasks.Builder;
import net.sf.json.JSONObject;
import org.apache.commons.lang.BooleanUtils;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.StaplerRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * OpenshiftWrapperBuilder
 *
 * @author Jozef Štaffen
 */
public class OpenshiftWrapperBuilder extends Builder {
	private final String context;
	private final String war;
	private final String archive;
	private final Boolean java7;
	private final Boolean hotDeploy;
	private final Boolean enableJpda;
	private final List<Marker> markers = new ArrayList<Marker>();
	private final String properties;

    @DataBoundConstructor
    public OpenshiftWrapperBuilder(String context, String war, String archive, Boolean java7, Boolean hotDeploy, Boolean enableJpda, String properties) {
        this.context = context;
	    this.war = war;
	    this.archive = archive;
	    this.java7 = java7;
	    this.hotDeploy = hotDeploy;
	    this.enableJpda = enableJpda;
	    this.properties = properties;

	    if (BooleanUtils.isTrue(java7)) {
		    markers.add(Marker.JAVA7);
	    }
	    if (BooleanUtils.isTrue(hotDeploy)) {
		    markers.add(Marker.HOT_DEPLOY);
	    }
	    if (BooleanUtils.isTrue(enableJpda)) {
		    markers.add(Marker.ENABLE_JPDA);
	    }
    }

	public String getContext() {
		return context;
	}

	public String getWar() {
		return war;
	}

	public String getArchive() {
		return archive;
	}

	public List<Marker> getMarkers() {
		return markers;
	}

	public Boolean getJava7() {
		return java7;
	}

	public Boolean getHotDeploy() {
		return hotDeploy;
	}

	public Boolean getEnableJpda() {
		return enableJpda;
	}

	public String getProperties() {
		return properties;
	}

	@Override
    public boolean perform(AbstractBuild build, Launcher launcher, BuildListener listener) {
		Wrapper wrapper = new Wrapper(listener.getLogger(), build.getWorkspace().getRemote());

		try {
			wrapper.wrap(getContext(), getWar(), getArchive(), markers, properties);
		} catch (Exception e) {
			listener.getLogger().print(e);
			return false;
		}

        return true;
    }

    @Override
    public DescriptorImpl getDescriptor() {
        return (DescriptorImpl)super.getDescriptor();
    }

    @Extension
    public static final class DescriptorImpl extends BuildStepDescriptor<Builder> {

        public DescriptorImpl() {
            load();
        }

        @Override
        public String getDisplayName() {
            return "Wrap application to Openshift structure";
        }

        @Override
        public boolean isApplicable(Class type) {
            return true;
        }

        @Override
        public boolean configure(StaplerRequest staplerRequest, JSONObject json) throws FormException {
            save();
            return true;
        }
    }
}
