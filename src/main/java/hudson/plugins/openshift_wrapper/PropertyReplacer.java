package hudson.plugins.openshift_wrapper;

import groovy.lang.Writable;
import groovy.text.GStringTemplateEngine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class PropertyReplacer {

    public static void replaceProperties(String propertyFile, String contextTemplate, String target) throws IOException, ClassNotFoundException {
        Properties properties = new Properties();
        BufferedReader fileReader = new BufferedReader(new FileReader(propertyFile));
        properties.load(fileReader);
        fileReader.close();

        GStringTemplateEngine engine = new GStringTemplateEngine();
        Writable template = engine.createTemplate(new File(contextTemplate)).make(properties);
	    FileWriter fileWriter = new FileWriter(target);
	    template.writeTo(fileWriter);
	    fileWriter.close();
    }
}
