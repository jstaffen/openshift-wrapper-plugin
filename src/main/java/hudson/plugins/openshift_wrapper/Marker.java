package hudson.plugins.openshift_wrapper;

public enum Marker {
	JAVA7("java7"),
	HOT_DEPLOY("hot_deploy"),
	ENABLE_JPDA("enable_jpda");

	private final String mark;

	Marker(String mark) {
		this.mark = mark;
	}

	public String getMark() {
		return mark;
	}
}
