package hudson.plugins.openshift_wrapper;

import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.assertTrue;


public class WrapperTest {
	private static final String WORKSPACE = "openshiftWrapperWorkspace";

	private static final String CONTEXT = "context.xml";
	private static final String WAR = "file.war";
	private static final String ARCHIVE = "archive.tar.gz";

	private String virtualWorkspace;

	@Before
	public void createVirtualWorkspace() {
		virtualWorkspace = System.getProperty("java.io.tmpdir") + File.separator + WORKSPACE;
		new File(virtualWorkspace).mkdirs();
	}

	@Test
	public void testCreateArchive() throws IOException, ArchiveException, CompressorException, ClassNotFoundException {
		asWorkspaceFile(CONTEXT).createNewFile();
		asWorkspaceFile(WAR).createNewFile();
		Wrapper wrapper = new Wrapper(System.out, virtualWorkspace);

		wrapper.wrap(CONTEXT, WAR, ARCHIVE, Arrays.asList(Marker.JAVA7, Marker.ENABLE_JPDA, Marker.HOT_DEPLOY), null);

		assertTrue(asWorkspaceFile(ARCHIVE).isFile());
	}

	@Test
	public void testCreateArchiveWithoutContext() throws IOException, ArchiveException, CompressorException, ClassNotFoundException {
		asWorkspaceFile(WAR).createNewFile();
		Wrapper wrapper = new Wrapper(System.out, virtualWorkspace);

		wrapper.wrap(null, WAR, ARCHIVE, Arrays.asList(Marker.JAVA7, Marker.ENABLE_JPDA, Marker.HOT_DEPLOY), null);

		assertTrue(asWorkspaceFile(ARCHIVE).isFile());
	}

	@After
	public void removeVirtualWorkspace() throws IOException {
		FileUtils.deleteDirectory(new File(virtualWorkspace));
	}

	private File asWorkspaceFile(String fileName) {
		return new File(virtualWorkspace + File.separator + fileName);
	}
}
